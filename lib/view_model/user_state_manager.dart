import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpoddemo/models/user_model.dart';

import '../network/api_handler.dart';
class UserViewModel with ChangeNotifier{

  final userProvider = FutureProvider<List<UserModel?>>((ref) async {
    return ApiHandler().userListApi(1);
  });
}