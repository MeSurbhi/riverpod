import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../../utils/custom_colors.dart';


class Dialogs {

  static loader(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      barrierColor: CustomColors.white.withOpacity(0.3),
      context: context,
      builder: (context) {
        return const SpinKitDoubleBounce(
          size: 60,
          color: CustomColors.primary,
        );
      },
    );
  }
}
