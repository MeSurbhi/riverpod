import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpoddemo/models/user_model.dart';
import 'package:riverpoddemo/view/widgets/dialog.dart';

import '../view_model/user_state_manager.dart';

class UsersNotifierPage extends ConsumerWidget {
  const UsersNotifierPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    AsyncValue<List<UserModel?>> userData;

    userData = ref.watch(UserViewModel().userProvider);
    return Scaffold(
      appBar: AppBar(
        title: const Text('RiverPod Example'),
      ),
      body: userData.when(
              loading: () => Dialogs.loader(context),
              error: (err, stack) => Center(child: Text(err.toString())),
              data: (userData) {
                return ListView.builder(
                  itemCount: userData.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        ListTile(
                          leading: CircleAvatar(
                            backgroundImage:
                                NetworkImage(userData[index]!.downloadUrl!),
                          ),
                          title: Text(userData[index]!.author!),
                        )
                      ],
                    );
                  },
                );
              })

    );
  }
}
