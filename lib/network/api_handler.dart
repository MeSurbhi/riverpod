import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:riverpoddemo/models/user_model.dart';

import 'api_urls.dart';
import 'exceptions.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class ApiHandler {
  Dio? _dio;

  ApiHandler() {
    BaseOptions options =
        BaseOptions(receiveTimeout: 300000, connectTimeout: 300000);
    _dio = Dio(options);
    _dio!.interceptors.add(PrettyDioLogger(
        requestHeader: true,
        requestBody: true,
        responseBody: true,
        responseHeader: false,
        error: true,
        compact: true,
        maxWidth: 90));
  }

  /// User Api
  Future<List<UserModel?>> userListApi(int pageCount) async {
    try {
      Response res = await _dio!.get(
        ApiUrls.userApi + pageCount.toString(),
      );
      if (res.statusCode == 200) {
        var list = json.decode(res.data) as List<dynamic>;

        List<UserModel> userModel =
            list.map((model) => UserModel.fromJson(model)).toList();
        return userModel;
      } else {
        throw Exception("can't get data");
      }
    } on DioError catch (e) {
      final errorMessage = DioExceptions.fromDioError(e).toString();
      throw errorMessage;
    }
  }
}
